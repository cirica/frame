<?php

//
//spl_autoload_register(function ($className) {
////
////    set_include_path(get_include_path().DIRECTORY_SEPARATOR.'Libraries');
////    spl_autoload_extensions('Core.php');
////    spl_autoload_extensions('Controller.php');
////
////    spl_autoload_register();
//
//
//    require_once 'Libraries/' . $className . '.php';
//});


spl_autoload_register(function ($class) {

    echo $class;
    $sources = [
        "Libraries/{$class}.php",
        "Controller/{$class}Controller.php",
        "Repository/{$class}.php",
        "Model/{$class}.php",
    ];


    echo "Class: " . $class;
    foreach ($sources as $source) {


        if (file_exists(__DIR__ . DIRECTORY_SEPARATOR . $source)) {

            echo 'file' . __DIR__ . DIRECTORY_SEPARATOR . $source;
            echo "<br>";

            require_once $source;
        }
    }
});