<?php

//namespace Libraries;

class Core
{
    private $controller = 'DefaultController';
    private $method = 'indexAction';
    private $params = [];

    public function __construct()
    {
        $url = $this->parseUrl();
        $this->callToAction($url);

    }

    private function callToAction($url)
    {
        $classControllerName = ucfirst($url[0]) . 'Controller';

        $classFile = __DIR__ . '/../Controller/' . $classControllerName . '.php';

        if (file_exists($classFile)) {
            $this->controller = $classControllerName;

            require_once $classFile;

            unset($url[0]);
        } else {
            require_once __DIR__ . '/../Controller/' . $this->controller . '.php';
        }

        $className = 'Controller\\' . $this->controller;
        //nu cred ca intra
        if (!class_exists($className)) {
            throw new Exception('{$className} does not exist');
        }

        $classInstance = new $className;

        if (isset($url[1])) {
            if (method_exists($this->controller, $url[1] . 'Action')) {
                $this->method = $url[1] . 'Action';
                unset($url[1]);
            }
        }

        if (!empty($url)) {
            $this->params = array_values($url);
        }

        call_user_func([$classInstance, $this->method], $this->params);
    }


    /** assume url[0] is Controller, url[1] is method, the rest are params */
    private function parseUrl()
    {
        $url = $_GET['url'];
        $url = explode('/', $url);

        return $url;
    }
}