<?php

interface BaseRepository
{
    public function add($entity);
    public function update($entity);
    public function remove($entity);
    public function find($id);
    public function findAll();

}