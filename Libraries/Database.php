<?php

//namespace Libraries;

class Database
{
    private static $instance;

    public static function build()
    {
        $configFile =  __DIR__.'/../config/config.yml';

        $config = yaml_parse_file($configFile);

        $dbConfig = $config['database_connection'];

        if (is_null(self::$instance)) {
            try {
                $db = 'mysql:host=' . $dbConfig['host'] . ';dbname='.$dbConfig['database'];
                $user = $dbConfig['username'];
                $pass = $dbConfig['password'];

                self::$instance = new PDO($db, $user, $pass);
            } catch (PDOException $exception) {
                throw new \Exception($exception->getMessage());
            }
        }

        return self::$instance;
    }

    private function __construct()
    {
    }

    private function __clone()
    {
        // TODO: Implement __clone() method.
    }
    private function __sleep()
    {
        // TODO: Implement __sleep() method.
    }
    private function __wakeup()
    {
        // TODO: Implement __wakeup() method.
    }


    protected function createInstance($data, $className) {
        return $this->mapData($data, new $className);
    }

    protected function mapData($data, $object)
    {
        $fields = array_keys(get_object_vars($object));
        foreach ($fields as $field)
        {
            $methodName = 'set'.ucfirst($field);
            if(method_exists($object, $methodName))
            {
                call_user_func([$object, $methodName], $data[$field]);
            } else {
                throw new \Exception("cannot set Property for field: " . $field);
            }
        }

        return $object;
    }

}