<?php

namespace Controller;

//use Repository\UserRepository;

use BaseRepository;

class IndexController extends \Controller
{
    public function indexAction()
    {

        try {
            echo "I am a index";

            $userRepository = new \UserRepository();

            $user = $userRepository->find(1);

            var_dump($user);
        } catch (\Exception $exception) {
            echo "We have a new Exception: " . $exception->getMessage();
        }
    }
}